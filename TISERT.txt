IMPLEMENTACI�N DE UN SISTEMA DE INFORMACI�N PARA LA TIENDA DE CAMISETAS �TISERT�

Se desea desarrollar un sistema de informaci�n para gestionar los datos de los empleados, secciones, clientes, proveedores y art�culos que se venden en Tisert.
En Tisert, los empleados asesoran a los clientes, siendo estos �ltimos los que llevan a cabo la compra de su/s camiseta/s. 

En lo que concierne a los proveedores, queremos optimizar nuestros precios lo m�s posible. Cada camiseta nos es ofrecida por varios proveedores y cada proveedor cuenta con un amplio cat�logo de camisetas. Hemos detectado que en el tiempo, la misma camiseta es suministrada por un mismo proveedor siempre a precios distintos, por lo que queremos guardar dichos precios para hacer un seguimiento de las tendencias y establecer cu�les son los proveedores que nos ofrecen mejores condiciones. Necesitaremos adem�s conocer el cif, nombre y tel�fono del contacto del proveedor, direcci�n, tel�fono y t�rminos de pago.

Tisert identifica sus camisetas por el c�digo de art�culo junto con el c�digo de modelo de camiseta. Necesitamos saber adem�s el color, la talla, si es una edici�n limitada o no, la secci�n de la tienda a la que pertenece (hombre/mujer/ni�os) y su precio de venta.

Cuando un cliente accede a nuestra tienda se le asigna un vendedor y cada vez que ese cliente acuda, le atender� la misma persona. Sin embargo, cada empleado tendr� varios clientes en su agenda, siendo de inter�s las fechas en las que un vendedor ha atendido a un determinado cliente. 

Sobre nuestros empleados necesitaremos almacenar sus datos personales, antig�edad en la empresa y la comisi�n que les corresponde. Dicha comisi�n se calcula en base a la antig�edad. Si el empleado lleva de 1 a 5 meses en la empresa, tendr� comisi�n de tipo A. Si llevase de 6 a 12 meses, la misi�n ser�a de tipo B. Los empleados que superen los 12 meses en nuestra empresa tendr�n una comisi�n de tipo C. La antig�edad se almacenar� siempre en meses por motivos administrativos.

La tienda est� dividida en varias secciones, que se distinguen por la tem�tica de las camisetas que se pueden encontrar all�. Los empleados pueden estar asignados a m�s de una secci�n, ya que nuestra tienda es de peque�as dimensiones. Sin embargo, por la habitual carga de trabajo pueden encontrarse varios empleados por secci�n para atender a todos los clientes que acuden.

Una vez un empleado asesora a un cliente, este entra en nuestra base de datos con un c�digo de cliente, datos personales y tel�fono, que ser�n archivados con toda regularidad siguiendo las directrices que establece la agencia espa�ola de protecci�n de datos. 

Esperamos que este sistema ayude a centralizar, normalizar y regular la informaci�n que manejamos, as� como a dar un mejor servicio a nuestros clientes.